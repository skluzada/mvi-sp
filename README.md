## Explainablity of CNN models for medical data
*skluzada for ni-mvi 2021/22*

The goal of this semestral work was to apply and evaluate various state-of-the-art explainability methods on Convolutional Neural Networks trained for the covid-19 pneumonia detection from x-ray chest images.

### Contents
All the notebooks in this repository were run in the google colab environment
* **Train CNN models, Train Vision Transformer (ViT)** - notebooks in which the respective models are trained
* **CNN models evaluation** - notebook in which all the CNN models are evaluated
* **Explanations for trained ResNet50,  Explanations for trained ResNet50, Explanations for Transformer** - notebooks in which explanations of the respective models are produced
* **Explanations evaluation and visualisation** - notebook in which the explanations are evaluated and visualised

### Downloadable
Additional content can be downloaded via the google drive:
* **COVIDx8B full dataset:** https://drive.google.com/file/d/1-rpDjHHjtftBlJH5AKIToI2sBiEa-96N/view?usp=sharing *(Required for: Train CNN models, Train Vision Transformer (ViT), CNN models evaluation)*
* **COVIDx8B test dataset only:** https://drive.google.com/file/d/1P34QCUFwgc3i4y2FAfrbQa-VIN3Jdccz/view?usp=sharing *(Required for: Explanations for trained ResNet50,  Explanations for trained ResNet50, Explanations for Transformer, Explanations evaluation and visualisation)*
* **Fine-tuned models weighs:** https://drive.google.com/drive/folders/1BmVQnqJJsB0VrsO48SECyijD_jHWP-F3?usp=sharing *(Required for: CNN models evaluation, Explanations for trained ResNet50,  Explanations for trained ResNet50, Explanations for Transformer)*
* **Explanations for the trained ResNet50 model:** https://drive.google.com/drive/folders/1OeAuCZrVqUBJY6wR7hxS-8FnntE4I01o?usp=sharing
* **Explanations for the random ResNet50 model:** https://drive.google.com/drive/folders/1n-PQzS6yITkCXibam7eY_rp93gdcN3-p?usp=sharing
* **Explanations for the random ViT model:** https://drive.google.com/drive/folders/1Bam_xJbgJjmALJv8uJOqLQJPbwzaK9I0?usp=sharing
* **Folder with visualised explanations for the trained ResNet50 model:** https://drive.google.com/drive/folders/1El0hiJTPN_2SCE0HmYQ4iqUezDuG9Cqw?usp=sharing